﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Init
{
    public partial class Form1 : Form
    {
        //Propiedades de GridView
        const int Ancho_Col = 45;
        const int Altura_Fil = 45;
        const int Total_Esparcidas = 9;
        const int largoA = Ancho_Col * Total_Esparcidas + 3;

        public int total = 81; //Celdas de Sudoku

        public int[,] juego1 = new int[,] {
            {6,5,4,2,1,7,3,8,9},
            {5,4,2,1,3,9,8,7,6},
            {1,9,6,5,4,8,2,3,7},
            {2,6,5,2,3,5,4,9,9} };
        public int[,] juego2;   
        public int[,] juego3;   //Cinco Juegos Cabrónes
        public int[,] juego4;
        public int[,] juego5;


        public Form1()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text.Equals("Chosa de los Pequeñines (Cagado)")) Pequenines();
            if (comboBox1.Text.Equals("Gemelas Ardientes (Fácil)")) Ardientes();
            if (comboBox1.Text.Equals("Atrapa al Correcaminos (Moderado)")) Corre();
            if (comboBox1.Text.Equals("Para cagar ladrillos (Intermedio)")) Ladrillos();
            if (comboBox1.Text.Equals("Escupidera de Salti (Difícil)")) Salti();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        void Pequenines()
        {
            //Propiedades de Formato
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToResizeColumns = false;
            dataGridView1.AllowUserToResizeColumns = false;
            dataGridView1.AllowUserToResizeRows = false;
            dataGridView1.RowHeadersVisible = false;
            dataGridView1.ColumnHeadersVisible = false;
            dataGridView1.GridColor = Color.DarkGray;
            dataGridView1.DefaultCellStyle.BackColor = Color.AntiqueWhite;
            dataGridView1.ScrollBars = ScrollBars.None;
            dataGridView1.Size = new Size(largoA, largoA);
            dataGridView1.Location = new Point(50, 50);
            dataGridView1.Font = new System.Drawing.Font("Calibri", 16.2F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            dataGridView1.ForeColor = Color.DarkBlue;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.CellSelect;

            for(int a = 0; a < Total_Esparcidas; ++a )
            {
                DataGridViewTextBoxColumn texto = new DataGridViewTextBoxColumn();
                texto.MaxInputLength = 1;
                dataGridView1.Columns.Add(texto);
                dataGridView1.Columns[a].Name = "Col " + (a + 1).ToString();
                dataGridView1.Columns[a].Width = Ancho_Col;
                dataGridView1.Columns[a].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                DataGridViewRow fila = new DataGridViewRow();
                fila.Height = Altura_Fil;
                dataGridView1.Rows.Add(fila);
            }

            dataGridView1.Columns[2].DividerWidth = 2;
            dataGridView1.Columns[5].DividerWidth = 2;
            dataGridView1.Rows[2].DividerHeight = 2;
            dataGridView1.Rows[5].DividerHeight = 2;

            Controls.Add(dataGridView1);


            /* dataGridView1.ColumnCount = total;
             dataGridView1.RowCount= total;
             juego1 = new int[total, total];
             for(int a = 0; a < total; ++a)
             {                                
                     //dataGridView1.Rows[a].HeaderCell.Value = Convert.ToString(b + 1);

             }*/
        }

        void Ardientes()
        {

        }

        void Corre()
        {

        }

        void Ladrillos()
        {

        }

        void Salti()
        {

        }
    }
}
