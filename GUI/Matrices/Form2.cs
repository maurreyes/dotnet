﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Matrices
{
    public partial class Form2 : Form
    {
        int dimension;
        int columna1, fila1, columna2, fila2,columna3, fila3;
        int[,] matriz1,matriz2,matriz3;

        public bool False { get; private set; }

        public Form2()
        {
            InitializeComponent();            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }        

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {            
            
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            for (int a = 0; a < columna1; a++)
            {
                for (int b = 0; b < fila1; b++)
                {
                    dataGridView3[a, b].Value = int.Parse(dataGridView2[a, b].Value.ToString()) + int.Parse(dataGridView1[a, b].Value.ToString());
                }
            }
        }

        private void dataGridView1_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {

        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {

        }
       
        private void button3_Click(object sender, EventArgs e)
        {
            for (int a = 0; a < columna1; a++)
            {
                for (int b = 0; b < fila1; b++)
                {
                    dataGridView3[a, b].Value = int.Parse(dataGridView2[a, b].Value.ToString()) * int.Parse(dataGridView1[a, b].Value.ToString());                    
                }
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            fila1 = (int)numericUpDown1.Value;
            fila2 = (int)numericUpDown1.Value;
            fila3 = (int)numericUpDown1.Value;
            //------------------------------------
            columna1 = (int)numericUpDown1.Value;
            columna2 = (int)numericUpDown1.Value;
            columna3 = (int)numericUpDown1.Value;
            /*-----------------------------------*/
            dataGridView1.ColumnCount = columna1;
            dataGridView1.RowCount = fila1;

            dataGridView2.ColumnCount = columna2;
            dataGridView2.RowCount = fila2;

            dataGridView3.ColumnCount = columna3;
            dataGridView3.RowCount = fila3;

            matriz1 = new int[columna1, fila1];
            for(int a = 0; a < columna1; a++)
            {
                dataGridView1.Columns[a].Name = Convert.ToString(a + 1);
                for(int b = 0; b < fila1; b++)
                {
                    dataGridView1.Rows[b].HeaderCell.Value = Convert.ToString(b + 1);
                }
            }

            matriz2 = new int[columna2, fila2];
            for (int a = 0; a < columna2; a++)
            {
                dataGridView2.Columns[a].Name = Convert.ToString(a + 1);
                for (int b = 0; b < fila2; b++)
                {
                    dataGridView2.Rows[b].HeaderCell.Value = Convert.ToString(b + 1);
                }
            }

            matriz3 = new int[columna3, fila3];
            for (int a = 0; a < columna3; a++)
            {
                dataGridView3.Columns[a].Name = Convert.ToString(a + 1);
                for (int b = 0; b < fila3; b++)
                {
                    dataGridView3.Rows[b].HeaderCell.Value = Convert.ToString(b + 1);
                }
            }           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }
       
        void BuBin()
        {

        }

        void Euclidez()
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        /*private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int columns = dataGridView1.CurrentCell.ColumnIndex;
            int rows = dataGridView1.CurrentCell.RowIndex;

            try
            {
                int actual = Convert.ToInt32(dataGridView1.CurrentCell.Value);
                if (columns == 0 && rows == 0)
                {
                    matriz1[columns, rows] = actual;
                    dataGridView1[columns, rows].Value = matriz1[columns, rows];
                }
                else if (rows == 0 && actual > Convert.ToInt32(dataGridView1[columns - 1, rows].Value) && dataGridView1[columns - 1, rows].Value != "")
                {
                    matriz1[columns, rows] = actual;
                    dataGridView1[columns, rows].Value = matriz1[columns, rows];
                }
                else if (columns == 0 && actual > Convert.ToInt32(dataGridView1[columna1 - 1, rows - 1].Value) && dataGridView1[columna1 - 1, rows - 1].Value != "")
                {
                    matriz1[columns, rows] = actual;
                    dataGridView1[columns, rows].Value = matriz1[columns, rows];
                }
                else if (columns != 0 && actual > Convert.ToInt32(dataGridView1[columns - 1, rows].Value) && dataGridView1[columns - 1, rows].Value != "")
                {
                    matriz1[columns, rows] = actual;
                    dataGridView1[columns, rows].Value = matriz1[columns, rows];
                }
                else
                {
                    dataGridView1[columns, rows].Value = "";
                }
            }
            catch
            {
                dataGridView1[columns, rows].Value = "";
                MessageBox.Show("No se puede debe hacer eso.");
            }

            private void dataGridView2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
            {
                int columns2 = dataGridView2.CurrentCell.ColumnIndex;
                int rows2 = dataGridView2.CurrentCell.RowIndex;

                try
                {
                    int actual2 = Convert.ToInt32(dataGridView2.CurrentCell.Value);
                    if (columns == 0 && rows == 0)
                    {
                        matriz2[columns, rows] = actual2;
                        dataGridView1[columns, rows].Value = matriz2[columns, rows];
                    }
                    else if (rows == 0 && actual2 > Convert.ToInt32(dataGridView1[columns - 1, rows].Value) && dataGridView1[columns - 1, rows].Value != "")
                    {
                        matriz2[columns, rows] = actual2;
                        dataGridView1[columns, rows].Value = matriz2[columns, rows];
                    }
                    else if (columns == 0 && actual2 > Convert.ToInt32(dataGridView1[columna2 - 1, rows - 1].Value) && dataGridView1[columna2 - 1, rows - 1].Value != "")
                    {
                        matriz2[columns, rows] = actual2;
                        dataGridView1[columns, rows].Value = matriz2[columns, rows];
                    }
                    else if (columns != 0 && actual2 > Convert.ToInt32(dataGridView1[columns - 1, rows].Value) && dataGridView1[columns - 1, rows].Value != "")
                    {
                        matriz2[columns, rows] = actual2;
                        dataGridView1[columns, rows].Value = matriz2[columns, rows];
                    }
                    else
                    {
                        dataGridView1[columns, rows].Value = "";
                    }
                }
                catch
                {
                    dataGridView1[columns, rows].Value = "";
                    MessageBox.Show("No se puede debe hacer eso.");
                }*/
      }           
 }
