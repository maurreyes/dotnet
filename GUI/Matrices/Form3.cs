﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Matrices
{
    public partial class Form3 : Form
    {
        private int fila01;
        private int columna01;
        private int[,] matriz01;

        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {                   
                int search = Convert.ToInt32(InputB01.Text);
                String resultado = "";

                    int[,] matrizAux = new int[columna01, fila01];
                    for (int y = 0; y < columna01; y++)
                    {
                        dataGridView01.Columns[y].Name = Convert.ToString(y + 1);
                        for (int x = 0; x < fila01; x++)
                        {
                            dataGridView01.Rows[x].HeaderCell.Value = Convert.ToString(x + 1);
                            dataGridView01[y, x].Value = matriz01[y, x];
                        }
                    }
                    resultado = Buscar(matriz01, search);
                    MessageBox.Show(resultado);    
        }

        private string Buscar(int[,] matriz, int search)
        {
            bool flag = false;
            int leny = matriz.GetLength(0);
            int lenx = matriz.GetLength(1);
            int x = lenx - 1, y = 0;
            while (x >= 0 && y < leny)
            {
                if (matriz[y, x] == search)
                {
                    flag = true;
                    break;
                }
                else if (matriz[y, x] < search)
                {
                    y++;
                    x = lenx - 1;
                }
                else
                {
                    x--;
                }
            }
            if (flag)
            {
                return "El número '" + search + "' se encontró en [" +
                        (y + 1) + "],[" + (x + 1) + "]";
            }
            else
            {
                return "No se encontró el elemento";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            fila01 = (int)numericUpDown1.Value;
            columna01 = (int)numericUpDown1.Value;
            dataGridView01.ColumnCount = columna01;
            dataGridView01.RowCount = fila01;

            matriz01 = new int[columna01, fila01];
            for (int i = 0; i < columna01; i++)
            {
                dataGridView01.Columns[i].Name = Convert.ToString(i + 1);
                for (int j = 0; j < fila01; j++)
                {
                    dataGridView01.Rows[j].HeaderCell.Value = Convert.ToString(j + 1);
                }
            }
        }

        private void dataGridView01_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int columns = dataGridView01.CurrentCell.ColumnIndex;
            int rows = dataGridView01.CurrentCell.RowIndex;

            try
            {
                int actual = Convert.ToInt32(dataGridView01.CurrentCell.Value);
                if(columns == 0 && rows == 0)
                {
                    matriz01[columns, rows] = actual;
                    dataGridView01[columns, rows].Value = matriz01[columns, rows];
                }
                else if(rows == 0 && actual > Convert.ToInt32(dataGridView01[columns - 1, rows].Value) && dataGridView01[columns- 1, rows].Value != "")
                {
                    matriz01[columns, rows] = actual;
                    dataGridView01[columns, rows].Value = matriz01[columns, rows];
                }
                else if(columns == 0 && actual > Convert.ToInt32(dataGridView01[columna01 - 1, rows - 1].Value) && dataGridView01[columna01 - 1, rows -1].Value != "")
                {
                    matriz01[columns, rows] = actual;
                    dataGridView01[columns, rows].Value = matriz01[columns, rows];
                }
                else if(columns != 0 && actual > Convert.ToInt32(dataGridView01[columns - 1, rows].Value) && dataGridView01[columns - 1, rows ].Value != "")
                {
                    matriz01[columns, rows] = actual;
                    dataGridView01[columns, rows].Value = matriz01[columns, rows];
                }
                else
                {
                    dataGridView01[columns, rows].Value = "";
                }
            }
            catch
            {
                dataGridView01[columns, rows].Value = "";
                MessageBox.Show("No se puede debe hacer eso.");
            }
        }

        private void dataGridView01_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
        }

        private void dataGridView01_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

        }
    }
}
