﻿using System;
/*Mauricio Reyes Zamora
Ingenieria en Sistemas Computacionales*/
namespace Matrices
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            //int euclidazo( int a, int b);

            //Console.WriteLine("Hello World!");
            int opc = 0;

            do
            {
                System.Console.Clear();
                Console.WriteLine("M A T R I C E S  E N  P R A C T I C A.");
                Console.WriteLine("Parcial 1\n");
                Console.WriteLine("Escoge una opcion:");
                Console.WriteLine("1.- Busqueda Binaria nxn");
                Console.WriteLine("2.- Suma de Matrices");
                Console.WriteLine("3.- Multiplicación de Matrices");
                Console.WriteLine("4.- Euclidazo MCD");
                Console.WriteLine("5.- Acerca de...");
                Console.WriteLine("6.- Salir");
                opc = Convert.ToInt32(Console.ReadLine());
                if(opc != 0)
                {
                    Console.WriteLine("No es una opcion valida");
                }

                switch(opc)
                {
                    case 1:
                        System.Console.Clear();
                        ConjuntoMatricial ejer = new ConjuntoMatricial();
                        break;

                    case 2:
                        System.Console.Clear();
                        SumaMatricial suma = new SumaMatricial();
                        break;

                    case 3:
                        MultiMatricial multi = new MultiMatricial();
                        break;

                    case 4:
                        Console.WriteLine("E U C L I D A Z O ");
                        Console.WriteLine("Encontrar el Maximo Comun Divisor entre dos numeros");
                        Console.WriteLine("Ingresa el Primer numero");
                        int a = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Ingresa el Segundo numero");
                        int b = Convert.ToInt32(Console.ReadLine());
                        Euclidazo(ref a,b);
                        Console.WriteLine("Euclidazo de los dos numeros es:" + Euclidazo(ref a ,b));
                        Console.ReadKey();
                        break;

                    case 5:
                        Console.WriteLine("Por Mauricio Reyes en TecMM");
                        break;
                }

            } while (opc != 6);

        }

        static int Euclidazo(ref int a, int b) 
        {
            if(b==0)
            {
                return a;
            }
            else
            {
                return Euclidazo(ref b, a % b);
            }
        }
    }
}
