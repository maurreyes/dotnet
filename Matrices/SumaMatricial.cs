﻿using System;
namespace Matrices
{
    public class SumaMatricial
    {
        public int[,] matriz,matri2,matri3; //A,B,C
        public int tam,total; //Dimensiones y valores 
        public const char ast = '*'; // se usar para el muestreo


        public SumaMatricial()
        {
            int opc = 0;
            do
            {

                Console.WriteLine("S U M A  D E  M A T R I C E S");
                Console.WriteLine("Como deseas crear la primer Matriz?");
                Console.WriteLine("1.-Manual(Usuario define tamanio, valores y orden");
                Console.WriteLine("2.-Automatico(Usuario define unicamente tamanio y orden)");
                Console.WriteLine("3.-Regresar a Menu Principal");
                opc = Convert.ToInt32(Console.ReadLine());

                switch (opc)
                {
                    case 1:
                        Manual(); //PENDIENTE
                        break;

                    case 2:
                        Automatico(); //CASI COMPLETADO
                        break;
                }
            } while (opc != 3);
        }

        public void Manual()
        {

            Console.WriteLine("M A N U A L");
            Console.WriteLine("Defina las dimensiones del primer arreglo: ");
            tam = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("El arreglo quedaria de la siguiente manera: ");
            matriz = new int[tam, tam];
            char[,] muestra = new char[tam, tam];

            for (int a = 0; a <= tam - 1; a++)
            {
                for (int b = 0; b <= tam -1; b++)
                {
                    muestra[a, b] = ast;
                }
            }

            for (int a = 0; a <= tam - 1; a++)
            {
                for (int b = 0; b <= tam - 1; b++)
                {
                    Console.Write(" ["+ muestra[a,b]+ "] ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();

            System.Console.Clear();
            Console.WriteLine("Ingrese la posicion que se le indica: ");
            for (int a = 0; a <= tam - 1;a++)
            {
                for (int b = 0; b <= tam - 1; b++)
                {
                    Console.Write("Posicion: ["+ a +"] "+" ["+ b +"] ");
                    matriz[a, b] = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine();
            }
            System.Console.Clear();
            Console.WriteLine("La matriz 1 es la siguiente: ");
            for (int a = 0; a <= tam - 1;a++)
            {
                for (int b = 0; b <= tam - 1; b++)
                {
                    Console.Write(" [" + matriz[a, b] + "] ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
            Console.WriteLine("Presione cualquier tecla para continuar");
            System.Console.Clear();

            matri2 = new int[tam, tam];
            Console.WriteLine("Comencemos con la segunda matriz: ");
            Console.WriteLine("Ingrese la posicion que se indica: ");
            for (int a = 0; a <= tam - 1; a++)
            {
                for (int b = 0; b <= tam - 1; b++)
                {
                    Console.Write("Posicion: ["+ a +"] "+" ["+ b +"] ");
                    matri2[a, b] = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine();
            }

            Console.WriteLine("La primera matriz es la siguiente: ");
            for (int a = 0; a <= tam - 1; a++)
            {
                for (int b = 0; b <= tam - 1; b++)
                {
                    Console.Write(" [" + matriz[a, b] + "] ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("La segunda matriz es la siguiente: ");
            for (int a = 0; a <= tam - 1; a++)
            {
                for (int b = 0; b <= tam - 1;b++ )
                {
                    Console.Write(" ["+ matri2[a,b] +"] ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();

            matri3 = new int[tam, tam];
            Console.WriteLine("Y la suma de estos es: ");
            for(int a = 0; a <= tam - 1; a++)
            {
                for(int b = 0; b <= tam - 1;b++)
                {
                    matri3[a, b] = matriz[a, b] + matri2[a, b];
                }
            }

            for (int a = 0; a <= tam - 1; a++)
            {
                for (int b = 0; b <= tam - 1; b++)
                {
                    Console.Write(" ["+ matri3[a,b] +"] ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();


        }

        public void Automatico()
        {
            Console.WriteLine("A U T O M A T I C O");
            Console.WriteLine("Defina las dimensiones del primer arreglo: ");
            tam = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("El arreglo quedaria de la siguiente manera: ");
            total = 1;
            matriz = new int[tam, tam];

            for (int a = 0; a <= tam -1; a++)
            {
                for (int b = 0; b <= tam -1; b++)
                {
                    matriz[a, b] = total++;
                }
            }

            for (int a = 0; a <= tam -1; a++)
            {
                for (int b = 0; b <= tam -1; b++)
                {
                    Console.Write(" ["+ matriz[a,b]+ "] ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();

            Console.WriteLine("A U T O M A T I C O");
            Console.WriteLine("Defina las dimensiones del segundo arreglo: ");
            tam = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("El arreglo quedaria de la siguiente forma: ");
            total = 1;
            matri2 = new int[tam, tam];

            for (int a = 0; a <= tam - 1; a++)
            {
                for (int b = 0; b <= tam - 1; b++)
                {
                    matri2[a, b] = total++;
                }
            }

            for (int a = 0; a <= tam - 1; a++)
            {
                for (int b = 0; b <= tam - 1; b++)
                {
                    Console.Write(" ["+ matri2[a,b] +"] ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
                                
            Console.WriteLine("El arreglo 1 es el siguiente: ");
            for (int a = 0; a <= tam - 1; a++)
            {
                for (int b = 0; b <= tam - 1; b++)
                {
                    Console.Write(" ["+ matriz[a,b] +"] ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("El arreglo 2 es el siguiente: ");
            for (int a = 0; a <= tam - 1; a++)
            {
                for (int b = 0; b <= tam - 1; b++)
                {
                    Console.Write(" ["+ matri2[a,b] +"] ");
                }
                Console.WriteLine();
            }

            matri3 = new int[tam, tam];
            Console.WriteLine("La suma de arreglo 1 y arreglo 2 es el siguiente: ");
            for (int a = 0; a < tam; a++)
            {
                for (int b = 0; b < tam; b++)
                {
                    matri3[a, b] = matriz[a, b] + matriz[a, b];
                }
            }

            for (int a = 0; a <= tam - 1; a++)
            {
                for (int b = 0; b <= tam - 1; b++)
                {
                    Console.Write(" ["+ matri3[a,b] +"] ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();

        }

    }
}