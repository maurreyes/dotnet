﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using MySql.Data.MySqlClient; //Librerías SQL
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDI
{        
    public partial class Form2 : Form
    {
        public char separador = '"';
        public bool usuario;
        //private object builder;

        public Form2()
        {
            InitializeComponent();            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                textBox5.Hide();
                label6.Hide();
                textBox4.Hide();
                label5.Hide();
            }
            usuario = false;
            textBox5.Show();
            label6.Show();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                textBox5.Show();
                label6.Show();
                textBox4.Show();
                label5.Show();
            }
            usuario = false;
            textBox5.Hide();
            label6.Hide();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        /*public void conexion()
        {
            MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder(); //String de conexión        
            builder.Server = "localhost";
            builder.UserID = "root";
            builder.Password = "Fish3r.Windows";
            builder.Database = "escuela";
        }*/

        private void button1_Click(object sender, EventArgs e)
        {
            if(radioButton2.Checked)
            {
                MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder(); //String de conexión        
                builder.Server = "localhost";
                builder.UserID = "root";
                builder.Password = "Fish3r.Windows";
                builder.Database = "escuela";                
                MySqlConnection conn = new MySqlConnection(builder.ToString());
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = $"INSERT INTO alumno (ID, Nombre, ApPaterno, ApMaterno, Carrera) value ({textBox6.Text},{separador}{textBox1.Text}{separador},{separador}{textBox2.Text}{separador},{separador}{textBox3.Text}{separador},{separador}{textBox4.Text}{separador});";
                conn.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Alumno Registrado");
            }

            else if(radioButton1.Checked)
            {
                MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder(); //String de conexión        
                builder.Server = "localhost";
                builder.UserID = "root";
                builder.Password = "Fish3r.Windows";
                builder.Database = "escuela";
                MySqlConnection conn = new MySqlConnection(builder.ToString());
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = $"INSERT INTO Profesor (ID, Nombre, ApPaterno, ApMaterno, Profesion) value ({textBox6.Text},{separador}{textBox1.Text}{separador},{separador}{textBox2.Text}{separador},{separador}{textBox3.Text}{separador},{separador}{textBox5.Text}{separador});";
                conn.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Docente Registrado");
            }
        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
            /*Form2 form2 = new Form2();
            form2.*/
        }
    }
}
